# AWS Certificate Associate Developer #

## Certification process ##

* <https://kb.epam.com/pages/viewpage.action?spaceKey=EPMCACM&title=AWS+Certification+Training+Plans>

## Courses ##

* <https://learn.epam.com/detailsPage?id=7843b369-0918-4d3a-a227-5dfd92079930>
* <https://learn.epam.com/detailsPage?id=9ffa6ac1-9ed7-4dbb-b81c-a56a55bbc191>

## Voucher call ##

* <https://kb.epam.com/display/EPMCACM/Fast+Track+-+AWS+Certification+Program>
