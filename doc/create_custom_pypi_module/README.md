# Create custom pypi package #

The following is about how to create custom Python package available via pypi.

## References ##

## Prerequisites ##

* GitHub account
* pypi.org account
* test.pypi.org account
* Python3 interpreter (pyenv setup recommended)
* installed python modules `wheel` and `twine`

## Steps ##

* create GitHub repository
* create pypi.org account
* create test.pypi.org account
* install required modules

```bash
pip install wheel twine
```
