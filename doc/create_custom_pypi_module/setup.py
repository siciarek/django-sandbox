from setuptools import setup, find_packages

VERSION = "0.0.2"
AUTHOR = "Jacek Siciarek"
DESCRIPTION = "Yet another Python fractal lib"
LONG_DESCRIPTION = "Package long description."

setup(
    name="pyfrctl",
    version=VERSION,
    description=DESCRIPTION,
    long_description=LONG_DESCRIPTION,
    long_description_content_type="text/markdown",
    author=AUTHOR,
    url="https://github.com/siciarek/pypi-sandbox",
    author_email="siciarek@gmail.com",
    license="MIT",
    packages=find_packages(),
    install_requires=[],
    keywords="fractals",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3",
    ],
)
