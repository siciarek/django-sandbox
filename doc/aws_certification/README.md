# References #

* <https://aws.amazon.com/certification/>
* <https://aws.amazon.com/getting-started/?intClick=dc_navbar&refid=9ab5159b-247d-4917-a0ec-ec01d1af6bf9>
* <https://learn.epam.com/detailsPage?id=9ffa6ac1-9ed7-4dbb-b81c-a56a55bbc191>
* <https://learn.epam.com/detailsPage?id=7843b369-0918-4d3a-a227-5dfd92079930>

Install AWS CLI

```bash
brew install awscli
```

Configure

To get access key and secret read:

https://docs.aws.amazon.com/powershell/latest/userguide/pstools-appendix-sign-up.html

```bash
aws configure
```

Check

```bash
aws s3 ls s3://rhythm-and-code-storage/ --recursive --human-readable --summarize
```
