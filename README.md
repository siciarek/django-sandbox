# Django sandbox #

## Create project ##

We assume that you are in your project directory.

```bash
pipenv install django django-extensions djangorestframework fastapi uvicorn numpy pandas
pipenv install --dev notebook pytest pytest-cov line-profiler memory-profiler snakeviz radon faker
pipenv run django-admin startproject app .
```

Add the following to the `app/settings.py` `INSTALLED_APPS`

```json
INSTALLED_APPS = [
    "app",
    "django_extensions",
```

## Create superuser ##

```bash
pipenv run python ./manage.py reset_db --no-input
pipenv run python ./manage.py migrate
pipenv run python ./manage.py createsuperuser --username=jacek.siciarek --email=siciarek@gmail.com
```
