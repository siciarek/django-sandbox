"""API Views."""

from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.exceptions import NotFound


@api_view(["POST"])
def algorithm_endpoint(request):
    """
    {
        "name": "binary_search",
        "type": "iterative",
        "data": {
            "haystack": [2, 3, 5, 7, 9],
            "needle": 7
        }
    }
    """

    data = request.data

    match data["name"]:
        case "binary_search":
            from app.algorithms.binary_search import recursive, iterative

            if data["type"] not in ["iterative", "recursive"]:
                raise NotFound(
                    f"Type \"{data['type']}\" for algorithm \"{data['name']}\" is not supported."
                )

            solutions = {"recursive": recursive, "iterative": iterative}

            result = solutions[data["type"]].solution(**data["data"])

            return Response({**data, "result": result})
        case _:
            raise NotFound(f"Algorithm \"{data['name']}\" is not supported.")
