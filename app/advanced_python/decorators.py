def simple_decorator(func):
    def wrapper(*args, **kwargs):
        print(f"Simple decorator arguments: {args=}, {kwargs=}")
        result = func(*args, **kwargs)
        print(f"Simple decorator result:    {result=}")
        return result

    return wrapper


def parametrized_decorator(all_values: bool = False):
    def decorator(func):
        def wrapper(*args, **kwargs):
            # use input arguments
            if all_values:
                if args:
                    print("~ Called with", *args)
                else:
                    print("~ Called with", *kwargs.values())
            else:
                print("~ Called with", len(args) if args else len(kwargs), "args ~")

            result = func(*args, **kwargs)

            # use result:

            print(f"{result=}")

            return result

        return wrapper

    return decorator


@simple_decorator
def cube(n: int) -> int:
    """Put in the power."""
    return n * n * n


print(cube(10))

print()


@parametrized_decorator(all_values=True)
def power(n: int, pwr: int) -> int:
    """Put in the power."""
    return n**pwr


print(power(2, 4))
print(power(n=2, pwr=4))

print()
