import weakref


class C:
    pass


class CustomDict(dict):
    pass


print("REGULAR DICT:")

c_1 = C()
print(c_1)

d = dict()

d["key"] = c_1
print(d, len(d))
del c_1
print(d, len(d))

print("\nWEAK REFERENCE:")

c_2 = C()
print(c_2)

wvd = weakref.WeakValueDictionary()

wvd["key"] = c_2
print(dict(wvd), len(wvd))  # 1
del c_2
print(dict(wvd), len(wvd))  # 0
