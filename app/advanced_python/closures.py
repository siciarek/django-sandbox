"""
 * Local(L): Defined inside function/class
 * Enclosed(E): Defined inside enclosing functions(Nested function concept)
 * Global(G): Defined at the uppermost level
 * Built-in(B): Reserved names in Python built-in modules
"""


def not_a_closure(a: float, b: float):
    return a * b


def outer(x: float):
    def inner(y: float):
        return x * y

    return inner


double_val = outer(2)

if __name__ == "__main__":
    assert double_val(8) == 16
    print(not_a_closure.__closure__)
    print(outer.__closure__)
    print(double_val.__closure__)

    import grpahviz
