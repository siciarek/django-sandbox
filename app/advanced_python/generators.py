def power_of_two_gen(max_val = 0):
    """
    >>> for i in power_of_two_gen(100):
    ...    print(i)
    1
    2
    4
    8
    16
    32
    64
    """
    n = 0
    while 1:
        p = 2 ** n
        if p > max_val:
            break
        yield p
        n += 1


