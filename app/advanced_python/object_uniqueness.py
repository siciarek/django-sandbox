class Person:
    def __init__(self, first_name: str, last_name: str):
        self.first_name = first_name
        self.last_name = last_name

    def __eq__(self, other):
        return self.last_name == other.last_name and self.first_name == other.first_name

    def __hash__(self):
        return hash(repr(dict(first_name=self.first_name, last_name=self.last_name)))


a = Person(first_name="John", last_name="Doe")
b = Person(first_name="John", last_name="Doe")

assert a == b

assert {a, b} == {a}
