# base class; one method defined
class Base:
    def method(self):
        print("method called from Base")


# class 1; inherits from Base; overrides the method
class Class1:
    def method(self):
        print("method called from Class1")


# class 2; inherits from Base; overrides the method
class Class2:
    def method(self, argument):
        print(f"method called from Class2 {argument=}")


# subclass; inherits from both classes
class SubclassOk(Class2, Class1):
    pass


# instantiate the subclass
b = SubclassOk()
# print out the MRO for the subclass
print(SubclassOk.mro())
# call the method with an argument
b.method(245)


class Subclass(Class1, Class2):
    pass


# instantiate the subclass
b = Subclass()
# print out the MRO for the subclass
print(Subclass.mro())
# call the method with an argument
b.method(45)
