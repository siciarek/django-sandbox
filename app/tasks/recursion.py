"""Recursion sample solutions module."""


def fac(n: int):
    """
    Factorial recursive formula.

    >>> for i in range(10):
    ...      print(i, fac(n=i))
    0 1
    1 1
    2 2
    3 6
    4 24
    5 120
    6 720
    7 5040
    8 40320
    9 362880
    """
    return n * fac(n - 1) if n > 0 else 1


def fib_like(n: int, begin: dict):
    """
    Check fib like function for fibonacci numbers.

    >>> fib_like(n=0, begin={0: 1, 1: 1})
    1
    >>> fib_like(n=1, begin={0: 1, 1: 1})
    1
    >>> fib_like(n=2, begin={0: 1, 1: 1})
    2
    >>> fib_like(n=17, begin={0: 1, 1: 1})
    2584
    """
    if n in begin:
        return begin[n]

    result = 0

    for i in range(len(begin)):
        result += fib_like(n=n - i - 1, begin=begin)

    return result


def fib(n: int):
    """
    Fibonacci sequence generator:

    >>> for i in range(10):
    ...      print(i, fib(n=i))
    0 1
    1 1
    2 2
    3 3
    4 5
    5 8
    6 13
    7 21
    8 34
    9 55
    """

    return fib_like(n=n, begin={0: 1, 1: 1})


def trib(n: int):
    """
    Tribonacci sequence generator:

    >>> for i in range(10):
    ...      print(i, trib(n=i))
    0 0
    1 1
    2 1
    3 2
    4 4
    5 7
    6 13
    7 24
    8 44
    9 81
    """
    begin = {0: 0, 1: 1, 2: 1, 3: 2}

    if n in begin:
        return begin[n]

    return trib(n - 1) + trib(n - 2) + trib(n - 3)


def luc(n: int):
    """
    Lucas sequence generator:

    >>> for i in range(10):
    ...      print(i, luc(n=i))
    0 2
    1 1
    2 3
    3 4
    4 7
    5 11
    6 18
    7 29
    8 47
    9 76
    """

    return fib_like(n=n, begin={0: 2, 1: 1})


def pel(n: int):
    """
    Pell sequence generator:

    >>> for i in range(10):
    ...      print(i, pel(n=i))
    0 0
    1 1
    2 2
    3 5
    4 12
    5 29
    6 70
    7 169
    8 408
    9 985
    """

    begin = {0: 0, 1: 1}

    if n in begin:
        return begin[n]

    return 2 * pel(n - 1) + pel(n - 2)


def before(n: int):
    """
    >>> before(10)
    10-9-8-7-6-5-4-3-2-1-
    """
    if n == 0:
        return

    print(n, end="-")  # processing the value

    before(n - 1)


def after(n: int):
    """
    >>> after(10)
    1-2-3-4-5-6-7-8-9-10-
    """
    if n == 0:
        return

    after(n - 1)

    print(n, end="-")  # processing the value
