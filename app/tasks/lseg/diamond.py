# base class; one method defined
class Base:
    def method(self):
        print("method called from Base")


# class 1; inherits from Base; overrides the method
class Class1:
    def method(self):
        print("method called from Class1")


# class 2; inherits from Base; overrides the method
class Class2:
    def method(self, argument):
        print(f"method called from Class2 {argument=}")


# subclass; inherits from both classes
class Subclass(Class1, Class2):
    pass


# instantiate the subclass
b = Subclass()
# print out the MRO for the subclass
print(Subclass.mro())
# call the method with an argument
b.method(45)


# Basic Python knowledge: 40%
# Advanced Python knowledge (performance tricks, good practices): < 10%
# Python core libraries (itertools, collections): < 5%
# Docker and other tools: < 30%
# Cloud computing experience (AWS, others): < 5%
# Practical skills (live coding): < 15%
