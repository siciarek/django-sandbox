# --------------------------------------------------------------
# Special sorting
def frequency_sort(arr: list) -> list:
    """Sort the given list in the decreasing frequency order (number of times
    they appear in list)."""
    from collections import Counter

    result = sorted(Counter(arr).items(), reverse=True, key=lambda x: x[1])
    out = []
    for el, n in result:
        out.extend([el] * n)

    return out


assert frequency_sort([4, 6, 2, 2, 6, 4, 4, 4]) == [4, 4, 4, 4, 6, 6, 2, 2]
assert frequency_sort(["bob", "bob", "carl", "alex", "bob"]) == [
    "bob",
    "bob",
    "bob",
    "carl",
    "alex",
]
