WORD = "EFFECT"


def how_many_effects(input_string: str) -> int:
    """
    Get number of `effect` words could be created using characters from
    incoming string.

    Every letter should be used only once. Case and order unsensitive.
    """
    from collections import Counter
    from math import inf

    input_string = input_string.upper()
    counter_input = Counter(input_string)
    counter_word = Counter(WORD)

    result = inf

    for letter, count in counter_word.items():
        occurence = counter_input.get(letter, 0)

        if occurence < count:
            return 0

        how_many = occurence // count
        result = min(how_many, result)

    return result


assert how_many_effects("EFFCT") == 0
assert how_many_effects("efFecT") == 1
assert how_many_effects("cefeft") == 1
assert how_many_effects("eff 123 ect") == 1
assert how_many_effects("effeeffetctc.......") == 2
assert how_many_effects("eeeffect") == 1
assert how_many_effects("eeffctt") == 1
assert how_many_effects("eeffcccctt") == 1
