# --------------------------------------------------------------
# Is splitted?
def check_is_splitted(input_string: str, pattern_string) -> bool:
    """
    Check, if input string could be created by:

    - splitting pattern string to two pieces
        and
    - joining in reverse order?
    """
    from collections import deque

    l, a, b = len(input_string), deque(input_string), deque(pattern_string)

    if a == b:
        return False

    while l:
        l -= 1
        a.append(a.popleft())
        if a == b:
            return True

    return False


assert check_is_splitted("AAAcc", "ccAAA") is True
assert check_is_splitted("AAAcc", "AAAcc") is False
assert check_is_splitted("6789012345", "1234567890") is True
assert check_is_splitted("Hello world", "world Hello") is False
assert check_is_splitted("Hello world", "worldHello ") is True
