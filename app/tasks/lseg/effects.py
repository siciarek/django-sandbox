WORD = "EFFECT"


def how_many_effects(input_string: str) -> int:
    """
    Get number of `effect` words could be created using characters from
    incoming string.

    Every letter should be used only once. Case and order unsensitive.
    """


assert how_many_effects("EFFCT") == 0
assert how_many_effects("efFecT") == 1
assert how_many_effects("cefeft") == 1
assert how_many_effects("eff 123 ect") == 1
assert how_many_effects("effeeffetctc.......") == 2
