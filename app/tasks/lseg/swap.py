x = 4
y = 5

assert (x, y) == (4, 5)

# Your code goes here:

assert (x, y) == (5, 4), f"{(x, y)} != (5, 4)"
