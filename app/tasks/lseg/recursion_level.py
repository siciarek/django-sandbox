import sys


def climb_stairs(n: int, memo: dict = None):
    if memo is None:
        memo = [0] * (n + 1)
        memo[0] = 1

    if n < 0:
        pass

    elif memo[n] > 0:
        return memo[n]
    else:
        memo[n] = climb_stairs(n - 1, memo) + climb_stairs(n - 2, memo)

    return memo[n]


sys.setrecursionlimit(20000)
print(sys.getrecursionlimit())

print(climb_stairs(1000))
