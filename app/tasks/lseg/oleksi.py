# --------------------------------------------------------------
# Readonly attribute in class
class Test:
    """Trivial class."""

    def __init__(self):
        """Init."""
        self.var = 123


# >>> Test().var
# 123
#
# >>> Test().var = 1  # forbidden!
#
# --------------------------------------------------------------
# Parametrized decorator
@args_info(all_values=False)
def power(n: int, pwr: int) -> int:
    """Put in the power."""
    return n**pwr


#  >>> power(2, 4)
#  ~ Called with 2 args ~
#  16
#
#  >>> power(n=2, pwr=4)
#  ~ Called with 2 args ~
#  16
@args_info(all_values=True)
def power(n: int, pwr: int) -> int:
    """Put in the power."""
    return n**pwr


#  >>> power(2, 4)
#  ~ Called with: 2 4 ~
#  16
#
#  >>> power(n=2, pwr=4)
#  ~ Called with:  2 4 ~
#  16
# --------------------------------------------------------------
# How many `effect`'s?
WORD = "EFFECT"


def how_many_effects(input_string: str) -> int:
    """
    Get number of `effect` words could be created using characters from
    incoming string.

    Every letter should be used only once. Case and order unsensitive.
    """


assert how_many_effects("EFFCT") == 0
assert how_many_effects("efFecT") == 1
assert how_many_effects("cefeft") == 1
assert how_many_effects("eff 123 ect") == 1
assert how_many_effects("effeeffetctc.......") == 2


# --------------------------------------------------------------
# Is splitted?
def check_is_splitted(input_string: str, pattern_string) -> bool:
    """
    Check, if input string could be created by:

    - splitting pattern string to two pieces
        and
    - joining in reverse order?
    """


assert check_is_splitted("AAAcc", "ccAAA") is True
assert check_is_splitted("AAAcc", "AAAcc") is False
assert check_is_splitted("6789012345", "1234567890") is True
assert check_is_splitted("Hello world", "world Hello") is False
assert check_is_splitted("Hello world", "worldHello ") is True


# --------------------------------------------------------------
# Special sorting
def frequency_sort(arr: list) -> list:
    """Sort the given list in the decreasing frequency order (number of times
    they appear in list)."""


assert frequency_sort([4, 6, 2, 2, 6, 4, 4, 4]) == [4, 4, 4, 4, 6, 6, 2, 2]
assert frequency_sort(["bob", "bob", "carl", "alex", "bob"]) == [
    "bob",
    "bob",
    "bob",
    "carl",
    "alex",
]


# --------------------------------------------------------------
# Pig latin
def pig_it(sentence: str) -> str:
    """
    Move the first letter of each word to the end of it, then add "ay" to the
    end of the word.

    Leave punctuation marks untouched.
    """


assert pig_it("Pig latin is cool") == "igPay atinlay siay oolcay"
assert pig_it("Hello world !") == "elloHay orldway !"
