class Person:
    def __init__(self, first_name: str, last_name: str):
        self.first_name = first_name
        self.last_name = last_name


a = Person(first_name="John", last_name="Doe")
b = Person(first_name="John", last_name="Doe")

assert a == b, "Persons should not differ."
assert {a, b} == {a}, "Set should contain only unique Persons."
