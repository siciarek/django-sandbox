def args_info(all_values: bool = False):
    def decorator(function):
        def wrapper(*args, **kwargs):
            if all_values:
                if args:
                    print("~ Called with", *args)
                else:
                    print("~ Called with", *kwargs.values())
            else:
                print("~ Called with", len(args) if args else len(kwargs), "args ~")

            result = function(*args, **kwargs)
            return result

        return wrapper

    return decorator


@args_info(all_values=True)
def power(n: int, pwr: int) -> int:
    """Put in the power."""
    return n**pwr


print(power(2, 4))
print(power(n=2, pwr=4))
