# --------------------------------------------------------------
# Pig latin
def pig_it(sentence: str) -> str:
    """
    Move the first letter of each word to the end of it, then add "ay" to the
    end of the word.

    Leave punctuation marks untouched.
    """


assert pig_it("Pig latin is cool") == "igPay atinlay siay oolcay"
assert pig_it("Hello world !") == "elloHay orldway !"
