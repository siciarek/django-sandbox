def climb_stairs_m(n: int, memo: dict = None):
    if memo is None:
        memo = [0] * (n + 1)
        memo[0] = 1

    if n < 0:
        pass

    elif memo[n] > 0:
        return memo[n]
    else:
        memo[n] = climb_stairs(n - 1, memo) + climb_stairs(n - 2, memo)

    return memo[n]


def climb_stairs(n: int):
    if n < 0:
        return 0

    if n == 0:
        return 1

    return climb_stairs(n - 1) + climb_stairs(n - 2)


# climb_stairs = climb_stairs_m


tests = (
    (0, 1),
    (1, 1),
    (2, 2),
    (3, 3),
    (4, 5),
    (8, 34),
    (16, 1597),
    (32, 3524578),
    # (64, 17167680177565),
    # (128, 407305795904080553832073954),
    # (996, 10261062362033262336604926729245222132668558120602124277764622905699407982546711488272859468887457959087733119242564077850743657661180827326798539177758919828135114407499369796465649524266755391104990099120377),
)

for stairs, expected in tests:
    actual = climb_stairs(n=stairs)
    assert actual == expected, f"For {stairs=}, {expected=}, {actual=}"
