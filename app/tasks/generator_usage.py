def get_lines_as_list(filename: str):
    lines = []
    with open(filename) as fh:
        for text_line in fh:
            lines.append(text_line.rstrip())
    return lines


def get_lines_as_generator(filename: str):
    with open(filename) as fh:
        for text_line in fh:
            yield text_line.rstrip()


get_lines = get_lines_as_list

for line in get_lines("./find_min.py"):
    print(line)
