"""Nike interview task."""


def find_max(arr: list[int], minval: list[float]) -> int:
    """
    Given a list of integers
    1. Find the minimum value
    2. Use the recursive approach
    >>> numbers = [3, 5, 2, 39, 21, 18, 32, 9, 15, 6]
    >>> minimal_value = [float("-inf")]
    >>> find_max(arr=numbers, minval=minimal_value)
    >>> minimal_value.pop()
    39
    """
    mid = len(arr) >> 1

    if mid == 0:
        if arr[0] > minval[0]:
            minval[0] = arr[0]
    else:
        find_max(arr[:mid], minval)
        find_max(arr[mid:], minval)

