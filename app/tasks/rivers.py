"""
<p> You're given a two-dimensional array (a matrix) of potentially unequal
height and width containing only <span>0</span>s and <span>1</span>s. Each.

<span>0</span> represents land, and each <span>1</span> represents part
of a   river. A river consists of any number of <span>1</span>s that are
either   horizontally or vertically adjacent (but not diagonally
adjacent). The number   of adjacent <span>1</span>s forming a river
determine its size. </p>
"""

dim = (
    (0, -1),  # N
    (1, 0),  # E
    (0, 1),  # S
    (-1, 0),  # W
)


def flood(i, j, matrix, count=None):
    height: int = len(matrix)
    width: int = len(matrix[0])
    for offj, offi in dim:
        newi, newj = i + offi, j + offj
        if 0 <= newi < height and 0 <= newj < width and matrix[newi][newj] == 1:
            count[0] += 1
            matrix[newi][newj] = 0
            flood(i=newi, j=newj, matrix=matrix, count=count)


def riverSizes(matrix):
    height: int = len(matrix)
    width: int = len(matrix[0])
    rivindexes = []

    for i in range(height):
        for j in range(width):
            if matrix[i][j] == 0:
                continue
            matrix[i][j] = 0
            rivin = [1]
            flood(i, j, matrix, count=rivin)
            rivindexes.append(rivin[0])

    return rivindexes


if __name__ == "__main__":
    matrix = [
        [1, 0, 0, 1, 0],
        [1, 0, 1, 0, 0],
        [0, 0, 1, 0, 1],
        [1, 0, 1, 0, 1],
        [1, 0, 1, 1, 0],
    ]

    print(riverSizes(matrix=matrix))
