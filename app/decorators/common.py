from copy import deepcopy


def use_list_copy(func):
    def wrapper(*args, **kwargs):
        args, kwargs = deepcopy(args), deepcopy(kwargs)
        func(*args, **kwargs)
        return args[0]

    return wrapper
