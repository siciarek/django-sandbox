import naive_recursion
import dynamic_programming

solutions = [
    naive_recursion,
    dynamic_programming,
]

if __name__ == "__main__":
    capacity = 100
    val = [10, 30, 20]
    wt = [5, 10, 15]
    expected = 300

    for solution in solutions:
        actual = solution.solution(capacity=capacity, profits=val, weights=wt)
        print(
            actual,
            solution.__doc__,
        )
        assert actual == expected, f"{actual=} differs from {expected=}"
