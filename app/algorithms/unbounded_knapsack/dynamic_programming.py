"""Unbounded Knapsack, Dynamic Programming."""


def solution(capacity: int, profits: list[int], weights: list[int]):
    """Unbounded Knapsack, Dynamic Programming solution."""

    size: int = len(profits)

    dp: list[int] = [0] * (capacity + 1)

    for i in range(capacity + 1):
        for j in range(size):
            if weights[j] <= i:
                without_item = dp[i]
                with_item = dp[i - weights[j]] + profits[j]

                dp[i] = max(
                    without_item,
                    with_item,
                )

    return dp[-1]
