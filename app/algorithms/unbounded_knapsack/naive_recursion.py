"""Unbounded Knapsack, Naive Recursion."""


def solution(capacity: int, profits: list[int], weights: list[int], idx: int = None):
    """Unbounded Knapsack, Naive Recursion solution."""

    if idx is None:
        idx = len(weights) - 1

    if idx == 0:
        return (capacity // weights[0]) * profits[0]

    without_item = solution(capacity, profits, weights, idx - 1)
    with_item = float("-inf")

    if weights[idx] <= capacity:
        with_item = profits[idx] + solution(
            capacity - weights[idx], profits, weights, idx
        )

    return max(with_item, without_item)
