"""Merge sort."""


def merge_sort(arr: list):
    """Merge sort solution."""

    if len(arr) > 1:
        mid = len(arr) // 2

        left_side, right_side = arr[:mid], arr[mid:]

        merge_sort(left_side)
        merge_sort(right_side)

        i = j = k = 0

        # Copy data to temp arrays L[] and R[]
        while i < len(left_side) and j < len(right_side):
            if left_side[i] <= right_side[j]:
                arr[k] = left_side[i]
                i += 1
            else:
                arr[k] = right_side[j]
                j += 1
            k += 1

        # Checking if any element was left
        while i < len(left_side):
            arr[k] = left_side[i]
            i += 1
            k += 1

        while j < len(right_side):
            arr[k] = right_side[j]
            j += 1
            k += 1


def solution(arr: list):
    from copy import copy

    arr = copy(arr)
    merge_sort(arr)
    return arr
