"""Bubble sort."""


from app.decorators.common import use_list_copy


@use_list_copy
def solution(number: list) -> list:
    """Bubble sort solution."""

    n: int = len(number)

    for i in range(n):
        for j in range(1, n - i):
            if number[j - 1] > number[j]:
                number[j - 1], number[j] = number[j], number[j - 1]
    return number
