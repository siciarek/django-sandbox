"""Insertion sort."""

from app.decorators.common import use_list_copy


@use_list_copy
def solution(arr: list):
    # Traverse through all array elements
    for i, key in enumerate(arr):
        # Move elements of arr[0..i - 1], that are
        # greater than key, to one position ahead of their current position
        j = i - 1

        while j >= 0 and key < arr[j]:
            arr[j + 1] = arr[j]
            j -= 1

        arr[j + 1] = key
