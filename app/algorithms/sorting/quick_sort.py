"""Quick sort."""


def partition(arr: list, low: int, high: int):
    # Pointer for greater element
    i = low - 1

    # Traverse through all elements
    # compare each element with pivot
    for j in range(low, high):
        if arr[j] <= arr[high]:
            # If element smaller than pivot is found swap it with the greater element pointed by i
            i += 1

            # Swapping element at i with element at j
            arr[i], arr[j] = arr[j], arr[i]

    # Swap the pivot element with e greater element specified by i
    i += 1

    arr[i], arr[high] = arr[high], arr[i]

    # Return the position from where partition is done
    return i


def quick_sort(arr: list, low: int = 0, high: int = None):
    if high is None:
        high = len(arr) - 1

    if low < high:
        # Find pivot element index such that
        # element smaller than pivot index are on the left
        # element greater than pivot index are on the right
        pi = partition(arr, low, high)

        # Recursive call on the left of pivot
        quick_sort(arr, low, pi - 1)

        # Recursive call on the right of pivot
        quick_sort(arr, pi + 1, high)


def solution(arr: list):
    from copy import copy

    arr = copy(arr)
    quick_sort(arr)
    return arr
