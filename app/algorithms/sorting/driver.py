"""Sorting algorithms."""

# https://www.geeksforgeeks.org/sorting-algorithms/

import bubble_sort
import merge_sort
import quick_sort
import selection_sort
import insertion_sort

approaches = (
    bubble_sort,
    selection_sort,
    insertion_sort,
    merge_sort,
    quick_sort,
)

if __name__ == "__main__":
    from random import randint

    numbers = [randint(1, 250) for _ in range(24)]

    for approach in approaches:
        sorted_numbers = approach.solution(numbers)
        print(sorted_numbers, approach.__doc__)
        assert sorted(numbers) == sorted_numbers

    print(numbers, "SOURCE")
