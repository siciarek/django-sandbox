"""Disjoint Set - ready for Find Union algorithm."""


class DisjointSet:
    parent: dict

    def __repr__(self):
        return repr(self.parent)

    def __init__(self, n):
        self.parent = {i: i for i in range(n + 1)}

    def find(self, s):
        if s == self.parent[s]:
            return s
        self.parent[s] = self.find(self.parent[s])

        return self.parent[s]

    def union(self, u, v):
        self.parent[v] = u
