"""Binary search Recursive."""


def solution(needle: float, haystack: list[float], left: int = 0, right: int = None):
    """Binary search Recursive solution."""

    if right is None:
        right = len(haystack) - 1

    if left > right:
        return -1

    # To prevent from integer overflow, not necessary in Python but in other languages may help.
    middle = left + (right - left) // 2

    if needle == haystack[middle]:
        return middle

    if needle < haystack[middle]:
        return solution(needle=needle, haystack=haystack, left=left, right=middle - 1)

    return solution(needle=needle, haystack=haystack, left=middle + 1, right=right)
