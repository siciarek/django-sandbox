"""Binary search Iterative."""


def solution(haystack, needle):
    """Binary search Iterative solution."""

    left, right = 0, len(haystack) - 1

    while left <= right:
        # To prevent from integer overflow, not necessary in Python but in other languages may help.
        middle = left + (right - left) // 2

        if needle == haystack[middle]:
            return middle

        if needle < haystack[middle]:
            right = middle - 1
        else:
            left = middle + 1

    return -1
