"""Binary search driver script."""

import recursive
import iterative

solutions = (recursive, iterative)

if __name__ == "__main__":
    numbers = [2, 3, 5, 7, 9]
    find = 7
    expected = 3

    for solution in solutions:
        print(solution.__doc__)
        actual = solution.solution(needle=find, haystack=numbers)
        assert actual == expected, f"{actual=}, {expected=}"
