"""Jobs sequencing solutions driver."""

from job import Job
from app.algorithms.jobs_sequencing_with_deadline import (
    brute_force,
    greedy_approach,
    binary_search,
    disjoint_set,
)

approaches = (
    brute_force,
    greedy_approach,
    binary_search,
    disjoint_set,
)

if __name__ == "__main__":
    names = ["J1", "J2", "J3", "J4", "J5"]
    deadlines = [2, 1, 2, 3, 3]
    profits = [15, 10, 25, 5, 1]
    deadline = max(deadlines)

    jobs = []
    for idx, name in enumerate(names):
        jobs.append(Job(name=name, deadline=deadlines[idx], profit=profits[idx]))

    for approach in approaches:
        # https://www.geeksforgeeks.org/job-sequencing-problem/
        jobs = []
        arr = [
            ["a", 2, 100],  # Job Array
            ["b", 1, 19],
            ["c", 2, 27],
            ["d", 1, 25],
            ["e", 3, 15],
        ]

        arr = [
            ["Job 1", 5, 200],
            ["Job 2", 3, 180],
            ["Job 3", 3, 190],
            ["Job 4", 2, 300],
            ["Job 5", 4, 120],
            ["Job 6", 2, 100],
        ]

        arr = [
            ["J7", 2, 92],
            ["J1", 5, 85],
            ["J8", 3, 80],
            ["J5", 4, 55],
            ["J4", 3, 40],
            ["J2", 4, 25],
            ["J6", 5, 19],
            ["J3", 3, 16],
            ["J9", 7, 15],
        ]

        for args in arr:
            jobs.append(Job(*args))

        deadline = max((job.deadline for job in jobs))

        result = approach.solution(jobs=jobs)
        print(approach.__doc__, result)
