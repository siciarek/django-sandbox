"""Job module."""


class Job:  # pylint: disable=too-few-public-methods
    """Job class."""

    def __repr__(self):
        return f"{self.name} ({self.deadline}/{self.profit})"

    def __init__(self, name, deadline, profit):
        self.name = name
        self.deadline = deadline
        self.profit = profit
