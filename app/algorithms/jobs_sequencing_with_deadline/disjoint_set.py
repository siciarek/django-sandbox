"""Job Sequencing with Deadline, Disjoint Set."""

from job import Job
from app.algorithms.utils.disjoin_set import DisjointSet


# https://www.geeksforgeeks.org/job-sequencing-problem-using-disjoint-set/


def solution(jobs: list[Job]):
    """Job Sequencing with Deadline, Disjoint Set solution."""

    # Sort all jobs up to decreasing order of profit
    ordered_jobs = sorted(jobs, reverse=True, key=lambda x: x.profit)

    max_deadline = max((job.deadline for job in jobs))

    disjoin_set = DisjointSet(max_deadline)

    best_profit, result = 0, []

    for job in ordered_jobs:
        available_slot = disjoin_set.find(job.deadline)

        if available_slot > 0:
            disjoin_set.union(disjoin_set.find(available_slot - 1), available_slot)
            result.append(job)
            best_profit += job.profit

    return best_profit, result
