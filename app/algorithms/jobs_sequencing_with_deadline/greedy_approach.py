"""Job Sequencing with Deadline, Greedy Approach."""

from job import Job


# https://www.geeksforgeeks.org/job-sequencing-problem/
def solution(jobs: list[Job]):
    """Job Sequencing with Deadline, Greedy Approach solution."""

    # Sort all jobs up to decreasing order of profit
    ordered_jobs = sorted(jobs, reverse=True, key=lambda x: x.profit)

    max_deadline = max((job.deadline for job in jobs))

    slots = [False] * max_deadline

    best_profit, result = 0, []

    for job in ordered_jobs:
        # Find a free slot for this job (Note that we start from the last possible slot)
        last_possible_slot = min(max_deadline - 1, job.deadline - 1)

        for j in range(last_possible_slot, -1, -1):
            if slots[j] is False:
                slots[j] = True
                result.append(job)
                best_profit += job.profit
                break

    return best_profit, result
