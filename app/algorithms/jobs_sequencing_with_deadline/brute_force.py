"""Job Sequencing with Deadline, Brute Force."""

from job import Job


# https://www.geeksforgeeks.org/job-sequencing-problem/
def solution(jobs: list[Job]):
    """Job Sequencing with Deadline, Brute Force solution."""

    max_deadline = max((job.deadline for job in jobs))

    n: int = len(jobs)
    queue = [[]]

    best_profit, result = 0, []

    while queue:
        curr = queue.pop(0)
        if len(curr) < n:
            queue.append(curr + [1])
            queue.append(curr + [0])
        else:
            slots = [False] * max_deadline
            temp = []

            for job in [o for idx, o in enumerate(jobs) if curr[idx]]:
                last_possible_slot = min(max_deadline - 1, job.deadline - 1)
                for j in range(last_possible_slot, -1, -1):
                    if slots[j] is False:
                        slots[j] = True
                        temp.append(job)
                        break

            if sum(curr) == len(temp):
                curr_profit = sum((o.profit for idx, o in enumerate(jobs) if curr[idx]))
                if curr_profit > best_profit:
                    best_profit = curr_profit
                    result = temp

    return best_profit, result
