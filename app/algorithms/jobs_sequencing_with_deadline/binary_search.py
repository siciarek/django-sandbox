"""Job Sequencing with Deadline, Binary Search."""

import bisect
from job import Job


# https://www.interviewbit.com/blog/job-sequencing-with-deadlines/


def solution(jobs: list[Job]):
    """Job Sequencing with Deadline, Binary Search solution."""

    max_deadline = max((job.deadline for job in jobs))

    # Sort all jobs up to decreasing order of profit
    ordered_jobs = sorted(jobs, reverse=True, key=lambda x: x.profit)

    slots = list(range(1, max_deadline + 1))

    best_profit, result = 0, []

    for job in ordered_jobs:
        if len(slots) == 0:
            break

        if job.deadline < slots[0]:
            continue

        # Find slot index and remove the slot from the list of available slots:
        idx = bisect.bisect(slots, job.deadline) - 1
        slots.pop(idx)

        # Add job to the result list:
        result.append(job)
        best_profit += job.profit

    return best_profit, result
