"""Https://en.wikipedia.org/wiki/Binary_heap."""

# pylint: disable=missing-function-docstring
from operator import gt, lt


class BinaryHeap:
    """Binary heap, raw implementation for educational purposes only."""

    def __init__(self, min_heap: bool = False):
        self.data, self.size, self.cmp, self.inf = [], 0, gt, float("inf")
        if min_heap:
            self.cmp = lt
            self.inf = float("-inf")

    @staticmethod
    def parent(i):
        return (i - 1) // 2

    @staticmethod
    def left(i):
        return 2 * i + 1

    @staticmethod
    def right(i):
        return 2 * i + 2

    def insert(self, element: float):
        """
        Insert element to the binary heap.

        >>> heap = BinaryHeap()
        >>> heap.insert(100)
        >>> heap.insert(34)
        >>> heap.insert(2)
        >>> heap.insert(300)
        >>> heap.extract()
        300
        """
        # First insert the new key at the end
        self.size += 1
        self.data.append(element)
        i = self.size - 1

        # Fix the min heap property if it is violated
        while i != 0 and self.cmp(self.data[i], self.data[self.parent(i)]):
            # Swap node value with it's parent
            self.data[i], self.data[self.parent(i)] = (
                self.data[self.parent(i)],
                self.data[i],
            )
            i = self.parent(i)

    def extract(self):
        root = self.data[0]
        self.data[0] = self.data[-1]
        self.data.pop()
        self.size -= 1
        self.heapify(0)
        return root

    def heapify(self, i):
        l, r, smallest = self.left(i), self.right(i), i

        if l < self.size and self.cmp(self.data[l], self.data[i]):
            smallest = l

        if r < self.size and self.cmp(self.data[r], self.data[smallest]):
            smallest = r

        if smallest != i:
            self.data[i], self.data[smallest] = self.data[smallest], self.data[i]
            self.heapify(smallest)

    def delete_key(self, i: int):
        self.decrease_key(i, self.inf)
        self.extract()

    def decrease_key(self, i: int, new_val: float):
        self.data[i] = new_val
        pi: int = self.parent(i)
        while i != 0 and self.data[pi] > self.data[i]:
            self.data[pi], self.data[i] = self.data[i], self.data[pi]

    def insert_then_extract(self, element):
        pass

    def search(self, element):
        pass

    def delete(self, element):
        pass


def disp(data: list, idx: int, result: dict):
    # https://forum.graphviz.org/t/binary-tree-force-lonely-node-to-be-left-or-right/1159/4

    curr = idx

    result["nodes"].append(curr)

    left = BinaryHeap.left(curr)
    if left < len(data):
        result["nodes"].append(left)
        result["edges"].append([curr, left])
        disp(data, left, result)

    right = BinaryHeap.right(curr)
    if right < len(data):
        result["nodes"].append(right)
        result["edges"].append([curr, right])
        disp(data, right, result)

    result["nodes"] = sorted(set(result["nodes"]))
    result["edges"] = sorted(result["edges"], key=lambda x: x[0])


def display(heap: BinaryHeap):
    cp = [*heap.data]

    pow = 0
    while 1:
        if not cp:
            break
        x = 2 ** pow
        row = []

        while x:
            row.append(cp.pop(0))
            if not cp:
                break
            x -= 1
        pow += 1
        print(row)


if __name__ == "__main__":
    heap = BinaryHeap(min_heap=False)
    heap.insert(5)
    heap.insert(100)
    heap.insert(10)
    heap.insert(8)
    heap.insert(1)
    heap.insert(14)
    heap.insert(32)
    heap.insert(3)
    heap.insert(21)
    heap.insert(201)
    heap.insert(45)
    heap.insert(11)
    heap.insert(39)
    heap.insert(900)
    heap.insert(27)
    heap.insert(1027)
    # heap.insert(23)

    display(heap=heap)
    import graphviz

    result = {"nodes": [], "edges": []}
    disp(heap.data, 0, result)

    dot = graphviz.Graph(
        "Binary Heap",
        graph_attr={
            'nodesep': '.2',
            'ranksep': '.5'
        }
    )
    dot.attr(
        "node",
        fontname="Arial",
        fontsize="11",
        shape="circle",
        fixedsize="shape",
        width=".5",
    )

    for n in result.get("nodes"):
        dot.node(str(n), str(heap.data[n]))

    for a, b in result.get("edges"):
        dot.edge(str(a), str(b))

    dot.render("binary-heap.gv")
