from typing import Optional


class BinomialHeapNode:
    def __repr__(self):
        return repr(self.__dict__)

    def __init__(
        self,
        value=None,
        parent: Optional["BinomialHeapNode"] = None,
        sibling: Optional["BinomialHeapNode"] = None,
    ):
        self.value = value
        self.children = []
        self.sibling = None

        if parent:
            parent.children.append(self)

        if sibling:
            sibling.sibling = self


class BinomialHeap(BinomialHeapNode):
    pass


if __name__ == "__main__":
    root = BinomialHeap()

    print(root)
