"""
Binomial Heap Implementation in Python

* https://www.geeksforgeeks.org/binomial-heap-2/
* https://www.geeksforgeeks.org/memory-representation-of-binomial-heap/?ref=rp

"""
from operator import lt, gt
from copy import deepcopy
from typing import Any


class BinomialHeapNode:

    def __repr__(self):
        return repr({
            "key": self.key,
            "degree": self.degree,
            "parent": self.parent.key if self.parent else None,
            "siblings": self.siblings,
            "children": self.children,
        })

    def __init__(
            self,
            key: Any = None,
            degree: int = 0,
            parent: "BinomialHeapNode" = None,
            sibling: "BinomialHeapNode" = None,
    ):
        self.key = key
        self.degree = degree
        self.parent = None
        self.children = []
        self.siblings = []

        if parent:
            self.parent = parent
            parent.children.append(self)
            parent.degree += 1

        if sibling:
            sibling.siblings.append(self)


root = BinomialHeapNode()

frst = BinomialHeapNode(12, sibling=root)

scnd = BinomialHeapNode(7, sibling=frst)
BinomialHeapNode(25, parent=scnd)

thrd = BinomialHeapNode(15, sibling=scnd)
BinomialHeapNode(41, parent=BinomialHeapNode(28, parent=thrd))
BinomialHeapNode(33, parent=thrd)


x = eval(f"{root}")
import json
print(json.dumps(x, indent=4))