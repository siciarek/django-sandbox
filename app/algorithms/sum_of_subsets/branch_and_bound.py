"""Sum of Subsets, Branch and Bound."""
from collections import deque
from node import Node


# https://www.youtube.com/watch?v=kyLxTdsT8ws


def compute_value(arr: list, mask: list[int]):
    value = 0
    for i, val in enumerate(arr):
        if i >= len(mask):
            break
        if mask[i] == 0:
            continue
        value += val
    return value


def is_promising(node: Node, arr: list, target_sum: int | float):
    complement = sum(arr[len(node.path):])

    return all([
        node.value <= target_sum,
        node.value + complement >= target_sum
    ])


def solution(arr: list, m: int | float):
    n: int = len(arr)
    root_node = Node()
    queue = deque([root_node])
    hit = 1

    while queue:
        current = queue.popleft()

        with_item = current.path + [1]
        with_item_value = compute_value(arr=arr, mask=with_item)
        if with_item_value == m:
            return True, hit

        without_item = current.path + [0]
        without_item_value = compute_value(arr=arr, mask=without_item)
        if without_item_value == m:
            return True, hit

        with_node = Node(
            value=with_item_value, path=with_item
        )

        without_node = Node(
            value=without_item_value, path=without_item
        )

        if is_promising(
                node=with_node,
                arr=arr,
                target_sum=m,
        ):
            hit += 1
            queue.append(with_node)

        if is_promising(
                node=without_node,
                arr=arr,
                target_sum=m,
        ):
            hit += 1
            queue.append(without_node)

    return False, hit
