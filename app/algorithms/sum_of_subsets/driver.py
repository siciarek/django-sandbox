import branch_and_bound
import brute_force_queue

approaches = (
    brute_force_queue,
    branch_and_bound,
)

if __name__ == "__main__":
    numbers = [5, 10, 12, 13, 15, 18]
    m = 30

    for approach in approaches:
        actual = approach.solution(arr=numbers, m=m)
        print(approach.__doc__, actual)
