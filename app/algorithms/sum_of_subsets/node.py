"""Decision Tree Node."""


class Node:
    def __repr__(self):
        return f"{self.value}, {self.path}"

    def __init__(self, value=0, path: list[int] = None):
        self.value = value
        self.path = path if path else []
