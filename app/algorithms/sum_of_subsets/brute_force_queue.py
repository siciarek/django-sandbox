"""Sum of Subsets, Brute Force with queue."""
from collections import deque
from node import Node


def solution(arr: list, m: int | float):
    n: int = len(arr)
    queue = deque([Node()])
    hit = 1

    while queue:
        current = queue.popleft()

        if len(current.path) < n:
            with_item = current.path + [1]
            with_node = Node(
                path=with_item
            )
            hit += 1
            queue.append(with_node)

            without_item = current.path + [0]
            without_node = Node(
                path=without_item
            )
            hit += 1
            queue.append(without_node)
        else:
            current_sum = sum((arr[idx] * current.path[idx] for idx in range(n)))
            if current_sum == m:
                return True, hit

    return False, hit
