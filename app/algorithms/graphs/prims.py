"""Minimum Spanning Tree, Prims's algorithm."""
import sys

from dijkstra import min_distance


def solution(graph: list[list[int]]):
    """
    Minimum Spanning Tree, Prim's algorithm, solution.

    >>> solution(graph=[
    ...     [0, 2, 0, 6, 0],
    ...     [2, 0, 3, 8, 5],
    ...     [0, 3, 0, 0, 7],
    ...     [6, 8, 0, 0, 9],
    ...     [0, 5, 7, 9, 0]
    ... ])
    (16, [[0, 1, 2], [1, 2, 3], [0, 3, 6], [1, 4, 5]])
    """

    vertex_count = len(graph)

    key = [sys.maxsize] * vertex_count
    parent: list = [None] * vertex_count  # Array to store constructed MST
    # Make key 0 so that this vertex is picked as first vertex
    key[0] = 0
    mst_set = [False] * vertex_count

    for _ in range(vertex_count):
        # Pick the minimum distance vertex from the set of vertices not yet processed.
        # u is always equal to src in first iteration
        u = min_distance(dist=key, vertex_count=vertex_count, mst_set=mst_set)

        # Put the minimum distance vertex in the shortest path tree
        mst_set[u] = True

        # Update dist value of the adjacent vertices of the picked vertex only if the current
        # distance is greater than new distance and the vertex in not in the shortest path tree
        for v in range(vertex_count):
            # graph[u][v] is nonzero only for adjacent vertices of m mst_set[v] is false for vertices
            # not yet included in MST. Update the key only if graph[u][v] is smaller than key[v]
            if mst_set[v] is False and 0 < graph[u][v] < key[v]:
                key[v] = graph[u][v]
                parent[v] = u

    return sum(key), [
        [parent[i], i, graph[i][parent[i]]] for i in range(1, vertex_count)
    ]
