# Graph representation #

The following are popular graph representations for graph computations.

## List of Edges (lista krawędzi) ##

list of lists/tuples containing indexes or ids of vertices joined by the edge.

```
[
    [2, 1],
    [3, 2],
    [0, 3],
    [0, 1]
]
```

In weighted graphs can also contain the weight of the edge.

```
[
    [0, 1, -1],
    [0, 2, 4],
    [1, 2, 3],
    [1, 3, 2],
    [1, 4, 2],
    [3, 2, 5],
    [3, 1, 1],
    [4, 3, -3],
]
```

## Adjacency Matrix (macierz sąsiedztwa) ##

Undirected graph representation

```
[
    [0, 1, 1, 1, 0],
    [1, 0, 0, 1, 1],
    [1, 0, 0, 1, 0],
    [1, 1, 1, 0, 1],
    [0, 1, 0, 1, 0]
]
```

Undirected graphs can also be represented by triangular matrix (macierz trójkątna), because of their symmetry.

Lower (left) triangular matrix (dolna macierz trójkątna).

```
[
    [0, 0, 0, 0, 0],
    [1, 0, 0, 0, 0],
    [1, 0, 0, 0, 0],
    [1, 1, 1, 0, 0],
    [0, 1, 0, 1, 0]
]
```

Which can be implemented as following, to use less memory.

```
[
    [0],
    [1, 0],
    [1, 0, 0],
    [1, 1, 1, 0],
    [0, 1, 0, 1, 0]
]
```

Upper (right) triangular matrix (górna macierz trójkątna)

```
[
    [0, 1, 1, 1, 0],
    [0, 0, 0, 1, 1],
    [0, 0, 0, 1, 0],
    [0, 0, 0, 0, 1],
    [0, 0, 0, 0, 0]
]
```

Directed graph representation

```
[
    [0, 1, 1, 0, 0],
    [0, 0, 0, 1, 1],
    [0, 0, 0, 1, 0],
    [1, 0, 0, 1, 1],
    [0, 0, 0, 0, 0]
]
```

In weighted graphs instead of 1 can be used numerical weight of an edge.

```
[
    [0,  4,  0,  0,  0,  0, 0,  8,  0],
    [4,  0,  8,  0,  0,  0, 0, 11,  0],
    [0,  8,  0,  7,  0,  4, 0,  0,  2],
    [0,  0,  7,  0,  9, 14, 0,  0,  0],
    [0,  0,  0,  9,  0, 10, 0,  0,  0],
    [0,  0,  4, 14, 10,  0, 2,  0,  0],
    [0,  0,  0,  0,  0,  2, 0,  1,  6],
    [8, 11,  0,  0,  0,  0, 1,  0,  7],
    [0,  0,  2,  0,  0,  0, 6,  7,  0]
]
```

## Adjacency List (lista sąsiedztwa) ##

For non weighted graphs (undirected and directed). Here implemented as dictionary (Python).

```
{
    0: [1, 2, 3, 4],
    1: [0, 2],
    2: [0, 1],
    3: [0, 4],
    4: [0, 3],
    5: []
}
```

If we use 0-based vertice identification we can simplify it to the regular list:

```
[
    [1, 2, 3, 4],
    [0, 2],
    [0, 1],
    [0, 4],
    [0, 3],
    []
]
```

## Incidence Matrix (macierz incydencji) ##

Here we have 4 vertices and 6 edges in undirected graph.

```
[
    [ 1,  1,  0,  0,  1,  0],
    [ 1,  0,  1,  0,  0,  1],
    [ 0,  0,  0,  1,  1,  1],
    [ 0,  1,  1,  1,  0,  0]
]
```

In directed graphs ve can use negative values to describe start and end vertices.

```
[
    [ 1, -1,  0,  0,  1,  0],
    [-1,  0, -1,  0,  0, -1],
    [ 0,  0,  0,  1, -1,  1],
    [ 0,  1,  1, -1,  0,  0]
]
```

## References ##

* <https://www.javatpoint.com/graph-theory-graph-representations>
* <https://stackabuse.com/courses/graphs-in-python-theory-and-implementation/lessons/representing-graphs-in-code/>
* <https://en.wikipedia.org/wiki/Triangular_matrix>
