"""
Eulerian cycle and path detection in undirected graph.

>>> solution(graph={
...     0: [1, 2, 3, 4],
...     1: [0, 2],
...     2: [0, 1],
...     3: [0, 4],
...     4: [0, 3],
... })
2
"""


# A function used by isConnected
def dsf_traverse(graph: dict[int, list[int]], vertex: int, visited: list[bool]):
    # Mark the current node as visited
    visited[vertex] = True

    # Recur for all the vertices adjacent to this vertex
    for i in graph[vertex]:
        if not visited[i]:
            dsf_traverse(graph, i, visited)


def is_connected(graph):
    vertex_count: int = len(graph)

    # Mark all the vertices as not visited
    visited = [False] * vertex_count
    selected_vertex = 0
    #  Find a vertex with non-zero degree
    while selected_vertex < vertex_count:
        selected_vertex += 1
        if len(graph[selected_vertex]) > 0:
            break

    # If there are no edges in the graph, return true
    if selected_vertex in {0, vertex_count - 1}:
        return True

    # Start DFS traversal from a vertex with non-zero degree
    dsf_traverse(graph=graph, vertex=selected_vertex, visited=visited)

    # Check if all non-zero degree vertices are visited
    for i in range(vertex_count):
        if visited[i] is False and len(graph[i]) > 0:
            return False

    return True


def solution(graph: dict):
    """
    Empty graph, contains Eulerian cycle.

    >>> solution(graph=dict())
    2

    Graph contains Eulerian Cycle.

    >>> solution(graph={
    ...     0: [1, 2, 3, 4],
    ...     1: [0, 2],
    ...     2: [0, 1],
    ...     3: [0, 4],
    ...     4: [0, 3],
    ... })
    2

    Graph contains Eulerian Path.

    >>> solution(graph={
    ...     0: [1, 2, 3],
    ...     1: [0, 2],
    ...     2: [0, 1],
    ...     3: [0, 4],
    ...     4: [3],
    ... })
    1

    Graph contains no Eulerian Cycle nor Eulerian Path.

    >>> solution(graph={
    ...     0: [1, 2, 3],
    ...     1: [0, 2, 3],
    ...     2: [0, 1],
    ...     3: [0, 1, 4],
    ...     4: [3],
    ... })
    0
    """

    vertex_count = len(graph)

    # All vertices with non-zero degree are connected.
    # We don’t care about vertices with zero degree because they don’t
    # belong to Eulerian Cycle or Path (we only consider all edges).
    if not is_connected(graph=graph):
        return 0

    odd = len([1 for i in range(vertex_count) if len(graph[i]) & 1])

    # If odd count is 2, then Semi-eulerian (Eulerian Path).
    # If odd count is 0, then Eulerian (Eulerian Cycle)
    # If count is more than 2, then graph is not Eulerian
    # Note that odd count can never be 1 for undirected graph

    if odd == 0:
        return 2
    if odd == 2:
        return 1

    return 0
