"""
Dijkstra algorithm to find the shortest paths to all the vertices.
https://www.geeksforgeeks.org/bellman-ford-algorithm-dp-23/

>>> solution(graph=[
...     [0, 1, -1],
...     [0, 2, 4],
...     [1, 2, 3],
...     [1, 3, 2],
...     [1, 4, 2],
...     [3, 2, 5],
...     [3, 1, 1],
...     [4, 3, -3],
... ], source=0)
[0, -1, 2, -2, 1]
"""


def solution(graph: list[list[int]], source: int):
    """
    Abdul Bari examples https://www.youtube.com/watch?v=FtN3BYH2Zes.

    >>> solution(graph=[
    ...     [0, 1, 6],
    ...     [0, 2, 5],
    ...     [0, 3, 5],
    ...     [1, 4, -1],
    ...     [2, 1, -2],
    ...     [2, 4, 1],
    ...     [3, 2, -2],
    ...     [3, 5, -1],
    ...     [4, 6, 3],
    ...     [5, 6, 3],
    ... ], source=0)
    [0, 1, 3, 5, 0, 4, 3]

    >>> solution(graph=[
    ...     [2, 1, -10],
    ...     [3, 2, 3],
    ...     [0, 3, 5],
    ...     [0, 1, 4],
    ... ], source=0)
    [0, -2, 8, 5]

    >>> solution(graph=[
    ...     [2, 1, -10],
    ...     [3, 2, 3],
    ...     [0, 3, 5],
    ...     [0, 1, 4],
    ...     [1, 3, 5],
    ... ], source=0)
    Traceback (most recent call last):
    ValueError: Graph contains negative weight cycle.
    """

    # Compute vertex count:
    vertex_count = len(set([x[0] for x in graph] + [x[1] for x in graph]))

    # Step 1: Initialize distances from src to all other vertices as INFINITE
    distances = [float("Inf")] * vertex_count
    distances[source] = 0

    # Step 2: Relax all edges |vertex_count| - 1 times. A simple shortest path from src to any other vertex
    # can have at-most |vertex_count| - 1 edges
    for _ in range(vertex_count - 1):
        # Update distances value and parent index of the adjacent vertices of
        # the picked vertex. Consider only those vertices which are still in queue
        for u, v, weight in graph:
            if distances[u] != float("Inf") and distances[u] + weight < distances[v]:
                distances[v] = distances[u] + weight

    # Step 3: check for negative-weight cycles. The above step guarantees shortest distances if graph doesn't contain
    # negative weight cycle. If we get a shorter path, then there is a cycle.
    for u, v, weight in graph:
        if distances[u] != float("Inf") and distances[u] + weight < distances[v]:
            raise ValueError("Graph contains negative weight cycle.")

    return distances
