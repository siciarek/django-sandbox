"""
Hamiltonian cycle module contains only one function solution().

>>> solution(graph=[
...     [0, 1, 1, 1, 1],
...     [1, 0, 1, 1, 1],
...     [1, 1, 0, 1, 0],
...     [1, 1, 1, 0, 1],
...     [1, 1, 0, 1, 0],
... ])
(260, 12)
"""

from collections import deque, defaultdict


def solution(graph: list[list[int]]):
    """
    Find the Hamiltonian cycles in the graph represented by adjacency matrix.

    Found 8 hamiltonian cycles
    >>> solution(graph=[
    ...     [0, 1, 1, 0, 1],
    ...     [1, 0, 1, 1, 1],
    ...     [1, 1, 0, 1, 0],
    ...     [0, 1, 1, 0, 1],
    ...     [1, 1, 0, 1, 0],
    ... ])
    (202, 8)

    Found 2 hamiltonian cycles
    >>> solution(graph=[
    ...     [0, 1, 0, 1, 0],
    ...     [1, 0, 1, 1, 1],
    ...     [0, 1, 0, 0, 1],
    ...     [1, 1, 0, 0, 1],
    ...     [0, 1, 1, 1, 0],
    ... ])
    (158, 2)

    Found no hamiltonian cycle:
    >>> solution(graph=[
    ...     [0, 1, 0, 1, 0],
    ...     [1, 0, 1, 1, 1],
    ...     [0, 1, 0, 0, 1],
    ...     [1, 1, 0, 0, 0],
    ...     [0, 1, 1, 0, 0],
    ... ])
    (118, 0)
    """
    vertex_count: int = len(graph)
    hit = 1
    queue = deque([[]])

    good_hit = 0
    result = defaultdict(list)
    while queue:
        path = queue.popleft()

        if len(path) < vertex_count:
            for i in range(vertex_count):
                # omit duplicated and nonadjacent vertices.
                if i in path or (len(path) > 1 and graph[path[-1]][i] == 0):
                    continue
                hit += 1
                queue.append(path + [i])
        else:
            # First vertex in the path is adjacent to the last.
            found = graph[path[0]][path[-1]] == 1

            if found:
                for i in range(1, vertex_count):
                    # Check if every vertex and his predecessor are adjacent.
                    if graph[path[i - 1]][path[i]] == 1:
                        continue
                    found = False
                    break

            # Collect proper results.
            if found:
                good_hit += 1
                good_path = [1 + i for i in path]

                key = [*good_path]
                while 1:
                    key = key[1:] + key[:1]
                    if key[0] == 1:
                        good_path.append(good_path[0])
                        result[" -> ".join(map(str, key + key[:1]))].append(good_path)
                        break

    return hit, len(result)
