"""
Dijkstra algorithm to find the shortest paths to all the vertices.

>>> solution(graph=[
...     [0, 4, 0, 0, 0, 0, 0, 8, 0],
...     [4, 0, 8, 0, 0, 0, 0, 11, 0],
...     [0, 8, 0, 7, 0, 4, 0, 0, 2],
...     [0, 0, 7, 0, 9, 14, 0, 0, 0],
...     [0, 0, 0, 9, 0, 10, 0, 0, 0],
...     [0, 0, 4, 14, 10, 0, 2, 0, 0],
...     [0, 0, 0, 0, 0, 2, 0, 1, 6],
...     [8, 11, 0, 0, 0, 0, 1, 0, 7],
...     [0, 0, 2, 0, 0, 0, 6, 7, 0]
... ], source=0)
[0, 4, 12, 19, 21, 11, 9, 8, 14]
"""

import sys


# A utility function to find the vertex with
# minimum distance value, from the set of vertices
# not yet included in shortest path tree
def min_distance(dist, vertex_count, mst_set):
    # Initialize minimum distance for next node
    min_dist = sys.maxsize
    min_index = 0

    # Search not nearest vertex not in the shortest path tree
    for i in range(vertex_count):
        if dist[i] < min_dist and not mst_set[i]:
            min_index = i
            min_dist = dist[i]

    return min_index


def solution(graph: list[list[int]], source: int):
    vertex_count = len(graph)
    distances_from_source = [sys.maxsize] * vertex_count
    distances_from_source[source] = 0
    mst_set = [False] * vertex_count

    for _ in range(vertex_count):
        # Pick the minimum distance vertex from the set of vertices not yet processed.
        # x is always equal to src in first iteration
        u = min_distance(
            dist=distances_from_source,
            vertex_count=vertex_count,
            mst_set=mst_set,
        )

        # Put the minimum distance vertex in the shortest path tree
        mst_set[u] = True

        # Update dist value of the adjacent vertices of the picked vertex only if the current
        # distance is greater than new distance and the vertex in not in the shortest path tree
        for v in range(vertex_count):
            candidate_distance = distances_from_source[u] + graph[u][v]
            if (
                mst_set[v] is False
                and graph[u][v] > 0
                and candidate_distance < distances_from_source[v]
            ):
                distances_from_source[v] = candidate_distance

    return distances_from_source
