"""Brent's cycle detection algorithm."""

from node import Node


def detect_cycle(graph: Node) -> tuple[int, int]:
    """
    Graph without a loop:

    >>> graph = Node(10)
    >>> graph.next = Node(20)
    >>> graph.next.next = Node(30)
    >>> graph.next.next.next = Node(40)
    >>> graph.next.next.next.next = Node(50)
    >>> detect_cycle(graph=graph)
    False

    Graph with a loop:
    >>> graph = Node(10)
    >>> graph.next = Node(20)
    >>> graph.next.next = Node(30)
    >>> graph.next.next.next = Node(40)
    >>> graph.next.next.next.next = Node(50)
    >>> # adding a loop for the sake of this example
    >>> temp = graph
    >>> while temp.next is not None:
    ...     temp = temp.next
    >>> temp.next = graph
    >>> detect_cycle(graph=graph)
    True
    """
    """Brent's cycle detection algorithm."""

    tortoise, hare = graph, graph.next
    power, length = 1, 1

    while hare is not None and hare != tortoise:
        if length == power:
            tortoise = hare
            length = 0
            power <<= 1

        hare = hare.next
        length += 1

    return hare is not None

    # # Otherwise length stores
    # # actual length of loop.
    # # If needed, we can also
    # # print length of loop.
    # # print("Length of loop is ")
    # # print (length)
    #
    # # Now set first_pointer
    # # to the beginning and
    # # second_pointer to
    # # beginning plus cycle
    # # length which is length.
    # tortoise = hare = graph
    #
    # while length > 0:
    #     hare = hare.next
    #     length -= 1
    #
    # # Now move both pointers
    # # at same speed so that
    # # they meet at the
    # # beginning of loop.
    # while hare != tortoise:
    #     hare = hare.next
    #     tortoise = tortoise.next
    #
    # return tortoise
