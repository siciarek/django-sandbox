"""Floyd's algorithm for cycle detection in linked lists."""

from node import Node


def detect_cycle(graph: Node):
    """
    Graph without a loop:

    >>> graph = Node(10)
    >>> graph.next = Node(20)
    >>> graph.next.next = Node(30)
    >>> graph.next.next.next = Node(40)
    >>> graph.next.next.next.next = Node(50)
    >>> detect_cycle(graph=graph)
    False

    Graph with a loop:
    >>> graph = Node(10)
    >>> graph.next = Node(20)
    >>> graph.next.next = Node(30)
    >>> graph.next.next.next = Node(40)
    >>> graph.next.next.next.next = Node(50)
    >>> # adding a loop for the sake of this example
    >>> temp = graph
    >>> while temp.next is not None:
    ...     temp = temp.next
    >>> temp.next = graph
    >>> detect_cycle(graph=graph)
    True
    """
    tortoise: Node = graph
    hare: Node = graph

    while None not in (tortoise, hare, hare.next):
        tortoise, hare = tortoise.next, hare.next.next
        if tortoise == hare:
            return True

    return False
