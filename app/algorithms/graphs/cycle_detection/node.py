"""Linked list Node."""


class Node:
    """Linked list Node class."""

    def __init__(self, d):
        self.data = d
        self.next = None
