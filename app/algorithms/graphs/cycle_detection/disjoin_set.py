"""Find cycle in Undirected Graph, Disjoint Set - Find Union."""

# * https://www.oodlestechnologies.com/blogs/Understanding-Disjoint-Set-And-Their-Use-Cases-in-Computer-Science/
# * https://www.techiedelight.com/union-find-algorithm-cycle-detection-graph/


from collections import defaultdict
from typing import Sized, Iterable, Any

from app.algorithms.utils.disjoin_set import DisjointSet


class Graph:
    # Constructor
    def __init__(self, edges: Iterable):
        self.adj_list: defaultdict[Any, list] = defaultdict(list)

        # add edges to the undirected graph (add each edge once only to avoid
        # detecting cycles among the same edges, say x -> y and y -> x)
        for src, dst in edges:
            self.adj_list[src].append(dst)


def solution(graph: Iterable | Sized):
    """
    No cycle:

    >>> solution(graph=(
    ...     (0, 1),
    ...     (0, 6),
    ...     (0, 7),
    ...     (1, 2),
    ...     (1, 5),
    ...     (2, 3),
    ...     (2, 4),
    ...     (7, 8),
    ...     (7, 11),
    ...     (8, 9),
    ...     (8, 10),
    ... ))
    False

    Cycle exists.
    >>> solution(graph=(
    ...     (0, 1),
    ...     (0, 6),
    ...     (0, 7),
    ...     (1, 2),
    ...     (1, 5),
    ...     (2, 3),
    ...     (2, 4),
    ...     (7, 8),
    ...     (7, 11),
    ...     (8, 9),
    ...     (8, 10),
    ...     (10, 11),  # edge (10, 11) introduces a cycle in the graph
    ... ))
    True
    """
    n: int = len(graph)

    disjoint_set = DisjointSet(n=n)

    ug = Graph(edges=graph)

    for u in range(n):
        for v in ug.adj_list[u]:
            a, b = disjoint_set.find(u), disjoint_set.find(v)

            if a == b:
                return True

            disjoint_set.union(a, b)

    return False
