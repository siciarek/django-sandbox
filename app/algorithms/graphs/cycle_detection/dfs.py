"""Find cycle in Undirected Graph, DFS."""

# * https://www.oodlestechnologies.com/blogs/Understanding-Disjoint-Set-And-Their-Use-Cases-in-Computer-Science/
# * https://www.techiedelight.com/union-find-algorithm-cycle-detection-graph/


from collections import defaultdict
from typing import Iterable, Any


class Graph:
    # Constructor
    def __init__(self, edges: Iterable):
        self.adj_list: defaultdict[Any, list] = defaultdict(list)

        # add edges to the undirected graph (add each edge once only to avoid
        # detecting cycles among the same edges, say x -> y and y -> x)
        for src, dst in edges:
            self.adj_list[src].append(dst)


# A function used by DFS
def dfs_util(graph, v, visited):
    visited[v] += 1

    for neighbour in graph.adj_list[v]:
        if neighbour not in visited:
            dfs_util(graph, neighbour, visited)


def solution(graph):
    """
    No cycle:

    >>> solution((
    ...     (0, 1),
    ...     (0, 6),
    ...     (0, 7),
    ...     (1, 2),
    ...     (1, 5),
    ...     (2, 3),
    ...     (2, 4),
    ...     (7, 8),
    ...     (7, 11),
    ...     (8, 9),
    ...     (8, 10),
    ... ))
    None

    Cycle exists.
    >>> solution(graph=(
    ...     (0, 1),
    ...     (0, 6),
    ...     (0, 7),
    ...     (1, 2),
    ...     (1, 5),
    ...     (2, 3),
    ...     (2, 4),
    ...     (7, 8),
    ...     (7, 11),
    ...     (8, 9),
    ...     (8, 10),
    ...     (10, 11),  # edge (10, 11) introduces a cycle in the graph
    ... ))
    None
    # Traceback (most recent call last):
    # ValueError: Cycle deteected.
    """

    visited = defaultdict(int)

    dfs_util(graph=Graph(graph), v=2, visited=visited)

    # print(visited)

    return None
