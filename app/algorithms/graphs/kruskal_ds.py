"""Minimum Spanning Tree, Kruskal's algorithm, Disjoint Set - Find Union algorithm."""

from app.algorithms.utils.disjoin_set import DisjointSet


# https://www.geeksforgeeks.org/kruskals-minimum-spanning-tree-algorithm-greedy-algo-2/
def solution(graph: list[list]):
    """
    Minimum Spanning Tree, Kruskal's algorithm, Disjoint Set - Find Union algorithm solution.

    >>> solution(graph=[
    ...     [0, 2, 7],
    ...     [0, 4, 9],
    ...     [2, 1, 5],
    ...     [2, 3, 1],
    ...     [2, 5, 2],
    ...     [3, 5, 2],
    ...     [4, 5, 1],
    ...     [5, 1, 6],
    ... ])
    (16, [[2, 3, 1], [4, 5, 1], [2, 5, 2], [2, 1, 5], [0, 2, 7]])

    >>> solution(graph=[
    ...     [0, 1, 10],
    ...     [0, 2, 6],
    ...     [0, 3, 5],
    ...     [1, 3, 15],
    ...     [2, 3, 4],
    ... ])
    (19, [[2, 3, 4], [0, 3, 5], [0, 1, 10]])

    >>> solution(graph=[
    ...     [0, 1, 7],
    ...     [0, 3, 5],
    ...     [1, 2, 8],
    ...     [1, 4, 7],
    ...     [2, 4, 5],
    ...     [3, 4, 15],
    ...     [3, 5, 6],
    ...     [4, 5, 8],
    ...     [4, 6, 9],
    ...     [5, 6, 11],
    ... ])
    (39, [[0, 3, 5], [2, 4, 5], [3, 5, 6], [0, 1, 7], [1, 4, 7], [4, 6, 9]])
    """

    vertices = set()
    for edge in graph:
        vertices.update(edge[:2])
    vertices_count = len(vertices)

    # Step 1:  Sort all the edges in non-decreasing order of their weights.
    graph = sorted(graph, key=lambda o: o[2])
    disjoint_set = DisjointSet(n=vertices_count)
    min_cost = 0

    result = []

    for _ in range(vertices_count + 1):
        # Step 2: Pick the edge with the smallest weight.
        u, v, w = graph.pop(0)

        a, b = disjoint_set.find(u), disjoint_set.find(v)

        # If including this edge cause cycle, ignore it and go to the next.
        if a == b:
            continue

        # If not union edges and add edge to the result.
        disjoint_set.union(a, b)
        result.append([u, v, w])
        min_cost += w

    return min_cost, result
