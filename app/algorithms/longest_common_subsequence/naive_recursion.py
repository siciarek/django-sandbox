"""Longest Common Subsequence, Naive Recursion."""


def solution(first: str, second: str, i: int = 0, j: int = 0):
    """Longest Common Subsequence, Naive Recursion solution."""

    if i == len(first) or j == len(second):
        return 0
    elif first[i] == second[j]:
        return 1 + solution(first, second, i + 1, j + 1)
    else:
        return max(solution(first, second, i, j + 1), solution(first, second, i + 1, j))
