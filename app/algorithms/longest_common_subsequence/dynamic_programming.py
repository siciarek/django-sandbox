"""Longest Common Subsequence, Dynamic Programming."""


def solution(first: str, second: str):
    """Longest Common Subsequence, Dynamic Programming solution."""

    m, n = len(first), len(second)

    dp: list[list[int]] = [[0 for _ in range(n + 1)] for _ in range(m + 1)]

    for i in range(1, m + 1):
        for j in range(1, n + 1):
            if first[i - 1] == second[j - 1]:
                dp[i][j] = dp[i - 1][j - 1] + 1
            else:
                dp[i][j] = max(dp[i - 1][j], dp[i][j - 1])

    return dp[-1][-1]
