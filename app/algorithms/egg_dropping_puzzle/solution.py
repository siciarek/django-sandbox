from math import sqrt, ceil


def solution(floors: int):
    # x(x + 1) / 2 <= floors
    # x**2 + x - 2 * floors <= 0
    # delta = 1 - 4 * 1 * (- 2 * floors) = 1 + 8 * floors <= 801
    # result = ceil((-1 + sqrt(1 + 8 * floors)) / 2)

    return ceil((sqrt(8 * floors + 1) - 1) / 2)
