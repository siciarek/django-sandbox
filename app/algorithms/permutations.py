"""
Permutations algorithm with queue.

For educational purpose only. For production use permutations from
itertools module.
"""

from collections import deque


def solution(phrase: str):
    """
    # Permutation implementation

    # >>> solution(phrase="ABCD") == [
    # ...     'ABCD', 'ABDC', 'ACBD', 'ACDB', 'ADBC', 'ADCB',
    # ...     'BACD', 'BADC', 'BCAD', 'BCDA', 'BDAC', 'BDCA',
    # ...     'CABD', 'CADB', 'CBAD', 'CBDA', 'CDAB', 'CDBA',
    # ...     'DABC', 'DACB', 'DBAC', 'DBCA', 'DCAB', 'DCBA'
    # ... ]
    # True
    """
    queue = deque([""])
    n: int = len(phrase)

    while queue:
        current = queue.popleft()

        if len(current) < n:
            for ch in phrase:
                if ch in current:
                    continue
                queue.append(current + ch)
        else:
            return [current] + [*queue]
