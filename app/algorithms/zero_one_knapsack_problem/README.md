# `Dyskretny problem plecakowy` (`0/1 Knapsack Problem`)

## Podejścia

n - liczba elementów, c - rozmiar plecaka

* Brute Force O(2^n)
* Dynamic Programming O(n * c) - tylko dla wag będących liczbami całkowitymi
* Branch & Bound O(n), O(2^n)
* Least Cost Branch & Bound O(n), O(2^n)

## Referencje

* <https://www.youtube.com/watch?v=tKvAniEbeqM&t=1076s>
* <https://www.youtube.com/watch?v=JW-26PaDpds&t=588s>
* <https://www.youtube.com/watch?v=yV1d-b_NeK8>
* <http://www.cs.put.poznan.pl/mszachniuk/mszachniuk_files/lab_aisd/Szachniuk-ASD-t5.pdf>
