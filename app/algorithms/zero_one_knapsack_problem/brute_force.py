"""0/1 Knapsack Problem, Brute Force solution."""

from .item import Item
from .node import Node


def solve(items: list[Item], capacity: int):
    n: int = len(items)
    Node.items = items

    queue = [Node()]  # add tree root node

    hit = 1

    best_result: Node = Node()

    while queue:
        current = queue.pop(0)

        if len(current.selected) < n:
            with_item = Node(selected=[*current.selected, 1])
            hit += 1

            queue.append(with_item)

            without_item = Node(selected=[*current.selected, 0])
            hit += 1

            queue.append(without_item)
        else:
            if current.weight <= capacity and current.profit >= best_result.profit:
                best_result = current

    return best_result.profit, hit
