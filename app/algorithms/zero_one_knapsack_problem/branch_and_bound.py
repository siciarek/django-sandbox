"""0/1 Knapsack Problem, Branch and Bound solution."""

from .item import Item
from .node import Node


def compute_bound(items: list[Item], selected: list[int], capacity: float):
    """
    Compute bound for selected items list using Fractional Knapsack algorithm.

    Items list should be sorted before sorting by lambda item:
    item.profit / item.weight.
    """
    bound = 0.0
    for idx, item in enumerate(items):
        if idx < len(selected) and selected[idx] == 0:
            continue

        if item.weight <= capacity:
            capacity -= item.weight
            bound += item.profit
        else:
            bound += capacity * (item.profit / item.weight)
            break

    return bound


def solve(items: list[Item], capacity: int):
    items.sort(reverse=True, key=lambda item: item.profit / item.weight)

    Node.items = items

    queue = [Node()]  # add tree root node
    hit = 1

    best_result: float = float("-inf")

    while queue:
        current = queue.pop(0)

        with_item = Node(selected=[*current.selected, 1])
        hit += 1

        if with_item.weight <= capacity:
            if with_item.profit > best_result:
                best_result = with_item.profit

            if (
                compute_bound(
                    items=items, selected=with_item.selected, capacity=capacity
                )
                > best_result
            ):
                queue.append(with_item)

        without_item = Node(selected=[*current.selected, 0])
        hit += 1

        if without_item.weight <= capacity:
            if without_item.profit > best_result:
                best_result = without_item.profit

            if (
                compute_bound(
                    items=items, selected=without_item.selected, capacity=capacity
                )
                > best_result
            ):
                queue.append(without_item)

    return best_result, hit
