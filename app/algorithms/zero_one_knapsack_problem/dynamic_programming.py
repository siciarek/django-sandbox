"""0/1 Knapsack Problem, Dynamic Programming solution."""

from .item import Item


def solve(items: list[Item], capacity: int):
    n: int = len(items)
    dp: list[list[int]] = [[0 for _ in range(capacity + 1)] for _ in range(n + 1)]
    hit = 0

    for current_item in range(1, n + 1):
        for current_capacity in range(1, capacity + 1):
            item = items[current_item - 1]
            hit += 1

            if item.weight > current_capacity:
                dp[current_item][current_capacity] = dp[current_item - 1][
                    current_capacity
                ]
                continue

            dp[current_item][current_capacity] = max(
                # WITH ITEM:
                dp[current_item - 1][current_capacity - int(item.weight)]
                + int(item.profit),
                # WITHOUT ITEM:
                dp[current_item - 1][current_capacity],
            )

    return dp[n][capacity], hit
