import os

from app.algorithms.zero_one_knapsack_problem import (
    brute_force,
    dynamic_programming,
    branch_and_bound,
    least_cost_branch_and_bound,
)
from app.algorithms.zero_one_knapsack_problem.item import Item

if __name__ == "__main__":
    import json

    with open(
        os.path.join(
            os.path.dirname(__file__),
            "",
            "../..",
            "..",
            "tests",
            "data",
            "zero_one_knapsack_problem.json",
        )
    ) as fp:
        data = json.load(fp)

    data.sort(key=lambda x: len(x["p"]))

    for num, row in enumerate(data, 1):
        if num in (
            4,
            12,
        ):  # 4 contains float profits, 12 heavy load
            continue

        capacity, p, w, s = row["m"], row["p"], row["w"], row["s"]
        n = len(p)

        items: list[Item] = []
        for i, weight in enumerate(w):
            items.append(Item(weight=weight, profit=p[i]))

        expected_profit = sum([p[i] * bit for i, bit in enumerate(s)])

        kwargs = {"items": items, "capacity": capacity}

        # bf = 2 ** (n + 1) - 1
        # dp = n * m
        bf_result, bf = brute_force.solve(**kwargs)
        dp_result, dp = dynamic_programming.solve(**kwargs)
        bb_result, bb = branch_and_bound.solve(**kwargs)
        lc_bb_result, lc_bb = least_cost_branch_and_bound.solve(**kwargs)

        print(f"{num:3d} {n=:3d}, {bf=:9d}, {dp=:5d}, {bb=:6d}, {lc_bb=:5d}")

        assert bf_result == expected_profit, str(
            f"{num}, {bf_result=}, {expected_profit=}"
        )
        assert dp_result == expected_profit, str(
            f"{num}, {dp_result=}, {expected_profit=}"
        )
        assert bb_result == expected_profit, str(
            f"{num}, {bb_result=}, {expected_profit=}"
        )
        assert lc_bb_result == expected_profit, str(
            f"{num}, {lc_bb_result=}, {expected_profit=}"
        )
