"""0/1 Knapsack Problem, Least Cost Branch and Bound (LC-BB) solution."""

from .item import Item
from .node import Node


def compute_cost(items: list[Item], selected: list[int], capacity: float):
    """
    Compute cost  (fractional knapsack).

    Items list should be sorted before sorting by lambda item:
    item.profit / item.weight.
    """
    cost = 0.0
    for idx, item in enumerate(items):
        if idx < len(selected) and selected[idx] == 0:
            continue

        if item.weight <= capacity:
            capacity -= item.weight
            cost += item.profit
        else:
            cost += capacity * (item.profit / item.weight)
            break

    return cost


def compute_upper_bound(items: list[Item], selected: list[int], capacity: float):
    """
    Compute upper bound (0/1 knapsack - no fractions).
    Items list should be sorted before sorting by lambda item: item.profit / item.weight.
    """
    upper_bound = 0.0
    for idx, item in enumerate(items):
        if idx < len(selected) and selected[idx] == 0:
            continue

        if item.weight <= capacity:
            capacity -= item.weight
            upper_bound += item.profit
        else:
            break

    return upper_bound


def is_promising(candidate: Node, best_node: Node, capacity: float):
    if candidate.weight > capacity:
        return False

    if candidate.profit < best_node.profit:
        return False

    return True


def solve(items: list[Item], capacity: int):
    items.sort(reverse=True, key=lambda item: item.profit / item.weight)
    n: int = len(items)

    # For Least Cost Branch and Bound solution we need to use negative profit values.
    for item in items:
        item.profit *= -1

    Node.items = items

    upper = float("inf")

    root = Node()
    root.cost = compute_cost(items=items, selected=root.selected, capacity=capacity)
    root.upper_bound = compute_upper_bound(
        items=items, selected=root.selected, capacity=capacity
    )

    queue = [root]  # add tree root node
    hit = 1

    if root.upper_bound < upper:
        upper = root.upper_bound

    while queue:
        current = queue.pop(0)

        if len(current.selected) < n:
            with_item = Node(selected=[*current.selected, 1])
            hit += 1

            if with_item.weight <= capacity:
                with_item.cost = current.cost
                with_item.upper_bound = current.upper_bound

                if with_item.upper_bound < upper:
                    upper = with_item.upper_bound

                if with_item.cost <= upper and with_item.weight <= capacity:
                    queue.append(with_item)

            without_item = Node(selected=[*current.selected, 0])
            hit += 1

            if without_item.weight <= capacity:
                without_item.cost = compute_cost(
                    items=items, selected=without_item.selected, capacity=capacity
                )
                without_item.upper_bound = compute_upper_bound(
                    items=items, selected=without_item.selected, capacity=capacity
                )

                if without_item.upper_bound < upper:
                    upper = without_item.upper_bound

                if without_item.cost <= upper:
                    queue.append(without_item)

    return -upper, hit
