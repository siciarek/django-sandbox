"""Decision tree node."""
from .item import Item


class Node:
    items: list[Item] = None

    def __repr__(self):
        return repr(self.__dict__)

    def __init__(
        self,
        selected: list[int] = None,
        cost: float = float("inf"),
        upper_bound: float = float("inf"),
    ):
        self.selected: list[int] = selected if selected else []
        self.profit: float = sum(
            [
                self.items[idx].profit * select
                for idx, select in enumerate(self.selected)
                if idx < len(self.items)
            ]
        )
        self.weight: float = sum(
            [
                self.items[idx].weight * select
                for idx, select in enumerate(self.selected)
                if idx < len(self.items)
            ]
        )

        # To be used for LC-BB
        self.cost: float = cost
        self.upper_bound: float = upper_bound
