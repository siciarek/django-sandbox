class Item:
    def __repr__(self):
        return repr(self.__dict__)

    def __init__(self, profit: float, weight: float):
        self.weight = weight
        self.profit = profit
