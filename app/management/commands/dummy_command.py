"""Dummy command module"""

from django.core.management.base import BaseCommand
import requests
import json


def fetch_data():
    create_list = lambda x: [int(i) for i in x.split("\n") if i != ""]

    dispatch = {
        "c": lambda x: int(x),
        "w": create_list,
        "p": create_list,
        "s": create_list,
    }

    data = []

    for i in range(1, 8 + 1):
        results = {"urls": []}
        for key, func in dispatch.items():
            url = f"https://people.sc.fsu.edu/~jburkardt/datasets/knapsack_01/p{i:02d}_{key}.txt"
            results["urls"].append(url)
            resp = requests.get(url=url)
            results[key] = func(resp.text)
            data.append(results)
        print(results)

    with open("knapsack-01.json", "w") as fh:
        json.dump(data, fh, indent=4)


class Command(BaseCommand):
    help = "My shiny new management command."

    def add_arguments(self, parser):
        parser.add_argument(
            "-r",
            "--run",
            action="store_true",
            required=False,
            default=False,
            help="Real script run.",
        )

    def handle(self, *args, **options):
        from app.algorithms.lcs import lcsr

        frst, scnd = "xydwamdioioeckslkpdddd", "waiecspxxdxd"

        print(lcsr(frst, scnd))

        #
        # with open("knapsack-01.json", "r") as fh:
        #     data = json.load(fh)
        #
        # for row in data:
        #
        #     weights = row["w"]
        #     profits = row["p"]
        #     capacity = row["c"]
        #     expected = 0
        #
        #     for i, bit in enumerate(row["s"]):
        #         expected += profits[i] * bit
        #
        #     if expected in [13549094]:
        #         break
        #
        #     kwargs = dict(
        #         profits=profits,
        #         weights=weights,
        #         capacity=capacity
        #     )
        #
        #     actual = zokp_space_optimized(**kwargs)
        #
        #     assert expected == actual, f"{actual=}, {expected=}"
        #
        #     print(f"{actual}, {kp_greedy(**kwargs)}, {kp_greedy(**kwargs, continuous=True)}")
