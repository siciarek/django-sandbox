from django.db import models


class Artifact(models.Model):
    TYPE_WORTHLESS = "worthless"
    TYPE_REGULAR = "regular"
    TYPE_UNIQUE = "unique"
    TYPES = (
        TYPE_WORTHLESS,
        TYPE_REGULAR,
        TYPE_UNIQUE
    )
    name = models.CharField(max_length=128)
    type = models.CharField(max_length=16, default=TYPE_REGULAR, choices=((e, e) for e in TYPES))
    description = models.CharField(max_length=1024)
