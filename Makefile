up:
	pipenv run python ./manage.py runserver


du:
	@PIPENV_VERBOSITY=-1 pipenv run python ./manage.py dummy_command

setup: setup_project reset_db

setup_project:
	pipenv install --dev
	-pipenv run django-admin startproject app .

reset_db:
	pipenv run python ./manage.py reset_db --no-input
	pipenv run python ./manage.py makemigrations app
	pipenv run python ./manage.py migrate
	pipenv run python ./manage.py loaddata fixtures/auth.json

cu:
	pipenv run python ./manage.py createsuperuser --username=jacek.siciarek --email=siciarek@gmail.com
